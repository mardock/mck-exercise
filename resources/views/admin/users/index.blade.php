@extends('layouts.master_admin')
@section('content')
<div class="row">
    <div class="d-flex flex-wrap flex-stack pb-7">
        <div class="d-flex flex-wrap align-items-center my-1">
            <h3 class="fw-bolder fs-3 me-5 my-1">Listado de Usuarios</h3>
            <form action="{{ route('users.index') }}" method="GET">
                <div class="d-flex align-items-center position-relative my-1">
                    <span class="svg-icon svg-icon-3 position-absolute ms-3">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2" rx="1" transform="rotate(45 17.0365 15.1223)" fill="black" />
                            <path d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z" fill="black" />
                        </svg>
                    </span>


                <input type="text" id="kt_filter_search" class="form-control form-control-sm border-body bg-body w-200px ps-10" placeholder="Search" />

                </div>
            </form>
        </div>
        <div class="d-flex flex-wrap my-1">
            {{-- <div class="d-flex my-0">
                <a href="{{route('users.create')}}" class="btn btn-primary btn-sm">Nuevo</a>
            </div> --}}
        </div>
    </div>
    <div class="tab-content">
        <div id="kt_project_users_table_pane" class="tab-pane fade show active">
            <div class="card card-flush">
                <div class="card-body pt-0">
                    <div class="table-responsive">
                        <table id="kt_tables" class="table table-row-bordered table-row-dashed gy-4 align-middle fw-bolder">
                            <thead class="fs-7 text-gray-400 text-uppercase">
                                <tr>
                                    <th class="min-w-50px">ID</th>
                                    <th class="min-w-50px">Nombre</th>
                                    <th class="min-w-50px">Email</th>
                                    <th class="min-w-50px">Creado</th>
                                    {{-- <th class="min-w-50px text-end">Acciones</th> --}}
                                </tr>
                            </thead>
                            <tbody class="fs-6">
                                @foreach ($items as $i)
                                    <tr>
                                        <td>
                                            <span class="text-gray-700 fw-bold text-muted d-block fs-7">
                                                {{$i->id}}
                                            </span>
                                        </td>
                                        <td>
                                            <span class="text-gray-700 fw-bold text-muted d-block fs-7">
                                                {{$i->name}}
                                            </span>
                                        </td>
                                        <td>
                                            <span class="text-gray-700 fw-bold text-muted d-block fs-7">
                                                {{$i->email}}
                                            </span>
                                        </td>
                                        <td>
                                            <span class="text-gray-700 fw-bold text-muted d-block fs-7">
                                                {{$i->created_at}}
                                            </span>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('custom-scripts')
    <script src="{{ asset('panel_assets/js/tables.js') }}"></script>
@endpush
