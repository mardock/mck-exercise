@extends('layouts.master_admin')

@section('title')
    Usuario
@endsection

@section('content')
    <div class="row">
        <div class="col-12 col-lg-12">
            <div class="card h-md-100">
                <div class="card-header">
                    <h3 class="card-title">
                        Información General
                    </h3>
                    <div class="card-toolbar">
                        <div class="justify-content-center">
                        </div>
                    </div>
                </div>
                <div class="card-body pt-5">
                    <div class="row">
                        <div class="col-12 col-xl-4">
                            <div class="row mb-8">
                                <div class="col-xl-12">
                                    <div class="fs-6 fw-bold">
                                        <label for="name" class="required form-label">Clave</label>
                                    </div>
                                </div>
                                <div class="col-xl-12">
                                    <input type="text" class="form-control" placeholder="Nombre" id="name" name="name" value="{{$item->cvegeo}}" readonly/>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-xl-4">
                            <div class="row mb-8">
                                <div class="col-xl-12">
                                    <div class="fs-6 fw-bold">
                                        <label for="name" class="required form-label">Nombre</label>
                                    </div>
                                </div>
                                <div class="col-xl-12">
                                    <input type="text" class="form-control" placeholder="Nombre" id="name" name="name" value="{{$item->nom_agee}}" readonly/>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-xl-4">
                            <div class="row mb-8">
                                <div class="col-xl-12">
                                    <div class="fs-6 fw-bold">
                                        <label for="name" class="required form-label">Nombre Abreviado</label>
                                    </div>
                                </div>
                                <div class="col-xl-12">
                                    <input type="text" class="form-control" placeholder="Nombre" id="name" name="name" value="{{$item->nom_abrev}}" readonly/>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-xl-4">
                            <div class="row mb-8">
                                <div class="col-xl-12">
                                    <div class="fs-6 fw-bold">
                                        <label for="name" class="required form-label">Población</label>
                                    </div>
                                </div>
                                <div class="col-xl-12">
                                    <input type="text" class="form-control" placeholder="Nombre" id="name" name="name" value="{{$item->pob}}" readonly/>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-xl-4">
                            <div class="row mb-8">
                                <div class="col-xl-12">
                                    <div class="fs-6 fw-bold">
                                        <label for="name" class="required form-label">Población Femenina</label>
                                    </div>
                                </div>
                                <div class="col-xl-12">
                                    <input type="text" class="form-control" placeholder="Nombre" id="name" name="name" value="{{$item->pob_fem}}" readonly/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('custom-scripts')
<script>
    $('#city_id').on("change",function(){
        $('#neighboardhood_id option').remove();
        $.ajax({
            method:'GET',
            url: "/cities/"+$(this).val()+"/neighboardhoods"
        }).done(function(response){
            var opt = new Option("Selecciona una Comuna", null);
            $('#neighboardhood_id').append(opt);

            response.neighboardhoods.forEach(n => {
                var o = new Option(n.name, n.id);
                $("#neighboardhood_id").append(o)
            });
        });
    });
</script>
@endpush
