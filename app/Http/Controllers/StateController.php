<?php

namespace App\Http\Controllers;

use stdClass;
use Exception;
use App\Models\State;
use App\Http\Helper\Inegi;
use Illuminate\Http\Request;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;

class StateController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        $items = State::orderBy('nom_agee')->get();
        return \View::make('admin.states.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $data = Inegi::getStates();

        $states = [];
        foreach ($data->datos as $state) {
            $states[] = State::updateOrCreate([
                'nom_agee' => $state->nom_agee,
                'nom_abrev' => $state->nom_abrev,
            ], [
                'cve_agee' => $state->cve_agee,
                'cvegeo' => $state->cvegeo,
                'pob' => $state->pob,
                'pob_fem' => $state->pob_fem,
                'pob_mas' => $state->pob_mas,
                'viv' => $state->viv,
            ]);
        }

        session()->flash('success');
        return Response::make(['code' => 1]);
        // return Redirect::route('states.index')->with('success');
    }

    /**
     * Display the specified resource.
     */
    public function show(State $state): View
    {
        return \View::make('admin.states.show', ['item' => $state]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(State $state)
    {
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, State $state)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(State $state)
    {
        $state->delete();
        session()->flash('success');
        return Response::make(['code' => 1]);
    }
}
