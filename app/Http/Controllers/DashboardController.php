<?php

namespace App\Http\Controllers;

use App\Models\State;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function dashboard() : View{
        return \View::make('admin.dashboard');
    }
}
