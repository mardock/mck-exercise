<?php

namespace App\Http\Helper;

use Exception;
Use GuzzleHttp\Client;
use App\Helper\Helper;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ServerException;
use stdClass;

class Inegi
{
    private static $cookies;
    private static $client;

    public static function init()
    {
        self::$client = new Client([
            'base_uri' => env('API_INEGI_STATES', 'https://gaia.inegi.org.mx/'),
        ]);
    }

    public static function response($res)
    {
        return json_decode($res->getBody());
    }

    public static function getStates()
    {
        try{
            self::init();

            $res = self::$client->get('wscatgeo/mgee/');

            $res = self::response($res);

            return $res;

        }catch(Exception $e){
            $res = new stdClass;
            $res->datos = [];
            return $res;
        }
    }
}
