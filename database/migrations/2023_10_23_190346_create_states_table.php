<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('states', function (Blueprint $table) {
            $table->id();

            $table->string('cvegeo');
            $table->string('cve_agee');
            $table->string('nom_agee');
            $table->string('nom_abrev');
            $table->string('pob');
            $table->string('pob_fem');
            $table->string('pob_mas');
            $table->string('viv');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('states');
    }
};
